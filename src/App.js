import React from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import Home from './applicationPages/Home'
import AboutUs from './applicationPages/AboutUs'
import ContactUs from './applicationPages/ContactUs'
import LogIn from './applicationPages/LogIn'
import SignUp from './applicationPages/SignUp'

import {Dropdown, DropdownButton} from 'react-dropdown';
import './App.css';
import './css/bootstrap.css'
import './css/coming-sssoon.css'
import {dict} from './dictionary/dictionary-english-us.js'

function App() {

    return (
        <Router>
            <Route exact path="/" component={Home} />
            <Route path="/about-Us" component={AboutUs} />
            <Route path="/contact-Us" component={ContactUs} />
            <Route path="/sign-Up" component={SignUp} />
            <Route path="/log-In" component={LogIn} />
        </Router>
    );
}


export default App;
