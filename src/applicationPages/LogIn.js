import '../App';
import React from 'react';
import {dict} from "../dictionary/dictionary-english-us";
import us_flag from './../images/flags/US.png';
import uk_flag from './../images/flags/GB.png';

function LogIn() {
    return (
        <div className="App">
            <header className="App-header">

                <nav className="navbar navbar-transparent navbar-fixed-top" role="navigation">
                    <div className="container">
                        <div className="navbar-header">
                            <button type="button" className="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                <span className="sr-only">Toggle navigation</span>
                                <span className="icon-bar"></span>
                                <span className="icon-bar"></span>
                                <span className="icon-bar"></span>
                            </button>
                        </div>

                        <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul className="nav navbar-nav">

                                <li className="dropdown">
                                    <a href="#" className="dropdown-toggle" data-toggle="dropdown">
                                        <img src={us_flag}/> English(US) <b className="caret"></b>
                                    </a>
                                    <ul className="dropdown-menu">
                                        <li><a href="#"><img src={uk_flag}/> English(UK)</a></li>
                                    </ul>
                                </li>

                            </ul>
                            <ul className="nav navbar-nav navbar-right">
                                <li>
                                    <a href="/">
                                        <i className="fa"></i>
                                        {dict.get("Navigation bar home")}
                                    </a>
                                </li>
                                <li>
                                    <a href="/about-Us">
                                        <i className="fa"></i>
                                        {dict.get("Navigation bar about us")}
                                    </a>
                                </li>
                                <li>
                                    <a href="/contact-Us">
                                        <i className="fa"></i>
                                        {dict.get("Navigation bar contact us")}
                                    </a>
                                </li>
                                <li>
                                    <a href="sign-Up">
                                        <i className="fa"></i>
                                        {dict.get("Navigation bar sign up")}
                                    </a>
                                </li>
                                <li>
                                    <a href="log-In">
                                        <i className="fa"></i>
                                        <b>{dict.get("Navigation bar log in")}</b>
                                    </a>
                                </li>
                            </ul>

                        </div>
                    </div>
                </nav>
                <div className="main">

                    <div className="cover black" data-color="black"></div>

                    <div className="container">
                        <h1 className="logo cursive">

                        </h1>
                        <div className="overlay">
                            <div className="content logIn">
                                <h2 className="logo cursive logIn">
                                    Traveler
                                </h2>
                                <h4 className="logo cursive logIn simple">Log in</h4>

                                <div className="subscribe">
                                    {/*<h5 className="info-text">*/}
                                        {/*Search for your ideal destination and find similar trips by people like you.*/}
                                    {/*</h5>*/}
                                    <div className="row">
                                        <div className="col-md-4 col-md-offset-4 col-sm6-6 col-sm-offset-3 ">
                                            <form className="form-inline" role="form">
                                                <div className="form-group logIn">
                                                    <input type="search" className="form-control logIn user" placeholder="Username"/>
                                                </div>
                                                <div className="form-group logIn">
                                                    <input type="search" className="form-control logIn pass" placeholder="Password"/>
                                                </div>
                                                <button type="submit" className="btn btn-danger btn-fill"> {dict.get("Login")}</button>
                                            </form>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                {/*<img src={logo} className="App-logo" alt="logo" />*/}
                {/*<p>*/}
                {/*Explore the world!*/}
                {/*</p>*/}
                {/*<a*/}
                {/*className="App-link"*/}
                {/*href="https://reactjs.org"*/}
                {/*target="_blank"*/}
                {/*rel="noopener noreferrer"*/}
                {/*>*/}
                {/*Learn React*/}
                {/*</a>*/}
            </header>
            <script src="../vendor/jquery/jquery-3.2.1.min.js"></script>
            <script src="../vendor/animsition/js/animsition.min.js"></script>
            <script src="../vendor/bootstrap/js/popper.js"></script>
            <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>
            <script src="../vendor/select2/select2.min.js"></script>
            <script src="../vendor/daterangepicker/moment.min.js"></script>
            <script src="../vendor/daterangepicker/daterangepicker.js"></script>
            <script src="../vendor/countdowntime/countdowntime.js"></script>
            <script src="../js/main.js"></script>
        </div>

    )
}

export default LogIn;